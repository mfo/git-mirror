defmodule GitMirror.Mixfile do
  use Mix.Project

  def project do
    [
      app: :git_mirror,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {GitMirror.Application, []}
    ]
  end

  defp deps do
    [
      {:distillery, "~> 2.1"},
      {:jason, "~> 1.2"}
    ]
  end
end
