# git mirror

### example config.json
```
{
  "mirror_interval_seconds": 300,
  "repositories": [
    {
      "source": "http://<user>:<access_token>@some-server.net/path/to/repo.git",
      "target": "http://<user>:<access_token>@some-other-server.net/path/to/another-repo.git"
    }
  ]
}
```


### buid
```
docker build -t git-mirror .
```

### run
```
docker run -v ${pwd}/config:/config -t git-mirror
```

### run with exposed git repositories
```
docker run -v ${pwd}/config:/config -v ${pwd}/data:/data -t git-mirror
```