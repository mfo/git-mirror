defmodule ConfigReader do
  @moduledoc """
  This module reads the git mirror config.json
  """

  def read_config do
    "./config/config.json" |> File.read!() |> Jason.decode!(keys: :atoms)
  end

  def read_mirror_interval do
    read_config()[:mirror_interval_seconds]
  end
end
