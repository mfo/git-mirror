defmodule GitMirror.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      {GitMirror, []},
    ]

    opts = [strategy: :one_for_one, name: GitMirror.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
