defmodule GitHelper do
  @moduledoc """
  This module provides functions for git interaction
  """
  @remote_name "mirror-target"

  def exists(source) do
    source |> path |> File.exists?
  end

  def init(source, target) do
  
    unless "./data" |> File.exists? do
      "./data" |> File.mkdir
    end

    dir = source_dir(source)

    System.cmd("git", ["clone", "--mirror", source, dir], cd: "./data/")

    add_target_remote(source, target)

  end

  def mirror([%{:source => source, :target => target} | repositories]) do

    IO.puts("-- mirroring: #{source_dir(source)} --")

    unless exists(source) do
      init(source, target)
    end

    fetch_and_push(source)

    mirror(repositories)
  end

  def mirror([]), do: nil

  defp fetch_and_push(source) do
    path = path(source)
    System.cmd("git", ["fetch"], cd: path)
    System.cmd("git", ["remote", "prune", "origin"], cd: path)
    System.cmd("git", ["push", "--all", "--prune", @remote_name], cd: path)
  end

  defp path(source) do
    "./data/#{source_dir(source)}"
  end

  def source_dir(source) do
    source |> Path.basename()
  end

  defp add_target_remote(source, target) do
    File.open("#{path(source)}/config", [:append])
    |> elem(1)
    |> IO.binwrite("\n[remote \"#{@remote_name}\"]\n\t\turl = #{target}")
  end
end
