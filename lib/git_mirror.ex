defmodule GitMirror do
  @moduledoc """
  This module mirrors git repositories at a given interval.
  """

  use GenServer, restart: :temporary

  @doc """
  Starts the GenServer.
  """
  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok)
  end


  @doc """
  Initializes the GenServer state with the configured interval.
  """
  def init(:ok) do
    interval = ConfigReader.read_mirror_interval()
    IO.puts("Running mirroring every #{interval} seconds.")
    {:ok, interval*1000, 1000}
  end


  @doc """
  Updates the GenServer state when a new version of the module was loaded.
  """
  def code_change(_old_vsn, _state, _extra) do
    with {:ok, interval, _} <- init(:ok) do
      {:ok, interval}
    else
      other -> {:error, other}
    end
  end


  @doc """
  Runs after the timeout has expired.
  """
  def handle_info(:timeout, interval) do

    IO.puts("Starting mirroring: #{DateTime.utc_now() |> datetime_format}.")

    config = ConfigReader.read_config
    config[:repositories] |> GitHelper.mirror

    next_run = DateTime.utc_now() |> DateTime.add(interval, :millisecond) |> datetime_format
    IO.puts("Next run: #{next_run}.")

    {:noreply, interval, interval}
  end


  defp datetime_format(datetime) do
    datetime |> DateTime.truncate(:second) |> DateTime.to_iso8601()
  end

end
